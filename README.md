# Michael Hug's résumé

This git repository contains logic for making redistributables from LaTeX. Additionally the pipeline will do various syntax checks, dictionary word comparisons, call the  https://languagetool.org api, and finally post to GitLab Pages to make it public. 

## Build all human readable formats
:warning: You will need several packages installed on your local machine for this to work

cd into the cloned repo then run :

`make`

Language checks can be run with :

`make proofread`

The make version uses a private language server 

## License
Have at it.
