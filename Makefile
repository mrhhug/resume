PROLETARIAT=Michael_Hug
PREAMBLE=$(PPROLETARIAT).sty
BUILD=build
OUTPUT=public
DOTFILES=.config

# Needed a way to spellcheck and syntax check on change
LATEX=$(BUILD)/$(PROLETARIAT).tex.lastupdate
PREAMBLE=$(BUILD)/$(PROLETARIAT).sty.lastupdate

# Compile all human readable formats by default
all: ascii ps pdf txt html
	@echo "Systems with the required libraries can proofread with the 'proofread' target"

proofread: $(BUILD)/$(PROLETARIAT).machine
	languagetool --list-unknown --mothertongue en-US --language en-US --disable PUNCTUATION_PARAGRAPH_END,WHITESPACE_RULE --languagemodel ~/.languagetoolngrams --level 'PICKY' $<
	mv $(DOTFILES)/.spelling ./
	mdspell --ignore-acronyms --en-gb '*.md'
	mv .spelling $(DOTFILES)/

html: $(OUTPUT)/html/$(PROLETARIAT).html

txt: $(OUTPUT)/$(PROLETARIAT).txt

pdf: $(OUTPUT)/$(PROLETARIAT).pdf

ps: $(OUTPUT)/$(PROLETARIAT).ps

ascii: $(OUTPUT)/$(PROLETARIAT).ascii

dvi: $(BUILD)/$(PROLETARIAT).dvi

detex: $(BUILD)/$(PROLETARIAT).detex

latex: $(LATEX)

$(OUTPUT)/html/$(PROLETARIAT).html: $(LATEX)
	make4ht $(PROLETARIAT) --output-dir $(OUTPUT)/html
	mkdir -p $(BUILD)/html
	mv $(addprefix $(PROLETARIAT).,aux xref tmp 4tc 4ct idv lg dvi log css html) $(BUILD)/html

$(OUTPUT)/$(PROLETARIAT).txt: $(OUTPUT)/$(PROLETARIAT).pdf
	pdftotext -nopgbrk -layout $(OUTPUT)/$(PROLETARIAT).pdf $@

$(BUILD)/$(PROLETARIAT).machine: $(OUTPUT)/$(PROLETARIAT).pdf
	pdftotext -nopgbrk -eol dos -raw -nodiag $(OUTPUT)/$(PROLETARIAT).pdf $@

$(OUTPUT)/$(PROLETARIAT).pdf: $(BUILD)/$(PROLETARIAT).dvi
	dvipdfmx $(BUILD)/$(PROLETARIAT).dvi -o $@

$(OUTPUT)/$(PROLETARIAT).ps: $(BUILD)/$(PROLETARIAT).dvi
	dvips -q -o$@ $<

$(OUTPUT)/$(PROLETARIAT).ascii: $(BUILD)/$(PROLETARIAT).dvi
	dvi2tty -w115 $(BUILD)/$(PROLETARIAT).dvi -o $@

$(BUILD)/$(PROLETARIAT).dvi: $(LATEX)
	latex -interaction=errorstopmode -output-directory=$(BUILD) $(PROLETARIAT)

$(BUILD)/$(PROLETARIAT).detex: $(LATEX)
	detex -e minipage,tabular  $(PROLETARIAT) | awk NF > $@

$(LATEX): $(PREAMBLE) $(PROLETARIAT).tex
	CHKTEXRC='$(DOTFILES)' chktex $(PROLETARIAT).tex
	hunspell -p $(DOTFILES)/.hunspell_en_US -d en_US -r -t $(PROLETARIAT).tex
	touch $@

$(PREAMBLE): Michael_Hug.sty | build output
	CHKTEXRC='$(DOTFILES)' chktex $(PROLETARIAT).sty
	touch $@

output:
	mkdir -p $(OUTPUT)

build:
	mkdir -p $(BUILD)

# Clean logs and intermediate files
.PHONY: clean
clean:
	rm -fr $(BUILD)
	rm -fr $(OUTPUT)
